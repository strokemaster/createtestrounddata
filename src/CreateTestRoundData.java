import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.sql.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CreateTestRoundData {
	
	public static void main(String[] args) throws Exception {
            createRoundData(10);	
    }
	
	// This will create a list of entries for rounds, including all relevant shot data, for testing 
	// 1.) Choose a course
	// 2.) Choose how many holes you will play
	// 3.) Choose a Date
	// 4.) Create data for holes 1-18
	
	// Assume that the green is 15 yards wide
	// For every hole:
	// If the distance is less than 15 yards, use a putter
	// Table for putter
	// Select the club with the greatest max distance that does not exceed the distance to the hole
    // If the club is a pitching wedge
	
	//
	public static void createRoundData (int entries)
	{
		
		// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		// SQL Objects
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement roundStatement = null, shotStatement = null;

		// The List of all available UserIDs in the database
		List<String> userIDs = new ArrayList<String>();
		// The list of all available CourseIDs in the database
		List<String> courseIDs = new ArrayList<String>();
		
		// The list of Prepared Statements that should be used for shots
		// These have to be saved because of the RoundID Foreign key in the shots table
		List<PreparedStatement> shotStatements = new ArrayList<PreparedStatement>();

		// The mapping of (Club Name, Distance)
		Map<String, Integer> clubMap = new LinkedHashMap<String, Integer>();
		// The mapping of putting distance, probability of making putt
		Map<Integer, Double> putterMap = new LinkedHashMap<Integer, Double>();
		Set<Integer> puttDistances = null;
		Iterator puttIter = null;
		
		// Objects for creating the round and shot entries
		String courseName="", roundID="", parS = "", distanceS = "", club = "", holesRoundValue="", parRoundValue="", scoresRoundValue = "" ;

		java.sql.Date date= null;
		int numHoles=18;
		int courseID = 1, userID=1, score = 0, distance = 0, shotDistance = 0, par = 0, totalPar = 0, shots = 0, previousPuttDistance = 0, currentPuttDistance = 0, roll=0;
		boolean puttMade = false;
		
		// Random
		Random r = new Random();		
		
		try{
			// Create the SQL Connection
	        con = DriverManager.getConnection(url + dbName, userName, password);
	        // Add the club data and pull the IDs from the User and Course Tables
			addClubData(clubMap, putterMap);
	       
			// For the number of entries requested
			for (int i = 0; i < entries; i ++)
			{
				// Set the list of holes, pars and scores back to empty for this round
				holesRoundValue=""; parRoundValue=""; scoresRoundValue = "";
				totalPar = 0;
				
				// Create a random course, user and date
				// Only affect test entries.  5 Test Courses and 4 Test users are created in insert-test-data.sql
				int [] courseIDArray = {123456, 112355, 423786, 999999, 987654}; 
				courseID = courseIDArray[r.nextInt(5)];
				userID = r.nextInt(4) + 1;
				date = randomDate(2015,2);
		       
		        // The list of all par values for the course
				List<String> pars = new ArrayList<String>();
				//The list of all distances for the currently selected course
				List<String> distances = new ArrayList<String>();
		        				
				// Get Course Data
		        st = con.createStatement();
				//st = con.prepareStatement("SELECT CourseName, YARDAGE FROM Course WHERE ID like \"" + courseID + "\"");
				rs = st.executeQuery("SELECT CourseName, PAR, YARDAGE FROM Course WHERE ID like \"" + Integer.toString(courseID) + "\"");
				
				
				// Get the comma separated values for Pars and Distances
				while (rs.next()) {
					courseName = rs.getString(1);
					parS = rs.getString(2);
		         	distanceS = rs.getString(3);
				}				
				
				// Split the par string based on comma and add the elements to the List passed as an argument
				pars = Arrays.asList(parS.split(","));
				
				// Split the distances string based on comma and add the elements to the List passed as an argument
				distances = Arrays.asList(distanceS.split(","));
		        // End Course Data
				
				// Set the score to 0 before the round starts
				score = 0;
				
				// Initialize the list of statements for shot
				shotStatements = new ArrayList<PreparedStatement>();
				
				// For the number of holes set at the top of this method, do a simulation of that hole and insert data into Shot 
				for (int j = 0; j < numHoles; j ++)
				{
					
					// Get the information from the jth hole on the selected course
					par = Integer.parseInt(pars.get(j));
					distance = Integer.parseInt(distances.get(j));
					shots = 0;
					
					// Add the par of the current hole for the round insert statement
					
					
					// While the distance to the hole is greater than 0
					while (distance > 0)
					{					
						shots ++;

						// Create the statement for this shot and insert the round specific values
						shotStatement = con.prepareStatement("INSERT INTO Shot (RoundID, CourseName, UserID, Hole, Stroke, startPoint, endPoint, startDistance, endDistance, Yardage,  Terrain, Club) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
						//shotStatement.setString(1, roundID);
						shotStatement.setString(2, courseName);
						shotStatement.setInt(3, userID);
						shotStatement.setString(4, Integer.toString(j+1));				
						shotStatement.setString(5, Integer.toString(shots));
						// Insert "0,0" for start and endPoint until we figure out the logic
						shotStatement.setString(6, "0,0");
						shotStatement.setString(7, "0,0");
						// 8, 9, 10 are startDistance, endDistance, yardage
						shotStatement.setString(8, Integer.toString(distance));
						
						
						club = selectClub(distance, clubMap);
						shotStatement.setString(12, club);

						//System.out.println("Distance: " + distance + " Selected Club: " + club);
						
						// If the club is a putter, assume the shot went in with 100% (for now)
						// Replace with a method for later
						if (club.equals("Putter"))
						{
							// Get the set of distances from the puttermap, create an iterator and grab the first entry from it
							puttDistances = putterMap.keySet();
							puttIter = puttDistances.iterator();
							// This will be 15 unless I change it later
							previousPuttDistance = (Integer) puttIter.next();
							
							// Go over the remaining entries in the set until we are using the lowest ceiling possible
							while (puttIter.hasNext())
							{
								currentPuttDistance = (Integer) puttIter.next();
								// If the distance to the current hole is more or less than the current entry in the set, 
								// end the while loop and use the previous entry's probability for the putt calculation
								if (distance >= currentPuttDistance)
									break;
								previousPuttDistance = currentPuttDistance;
							}
							// Generate a random number between 0 and 1.  If it is greater than the probability  of
							// getting the ball in, then the putt went in							
							if (r.nextDouble() > (1-putterMap.get(previousPuttDistance)))
								puttMade=true;
							else
								puttMade=false;
							
							// If the putt was made, set the distance equal to the distance from the hole and set 
							// terrain to green
							if (puttMade)
							{
								shotStatement.setString(10, Integer.toString(distance));
								shotStatement.setString(11, "Green");
								distance = 0;
								//System.out.println("You made a putt");
							}
							else
							{
								shotStatement.setString(10, Integer.toString(distance/2));
								shotStatement.setString(11, "Green");
								//System.out.println("Wah Wah - You missed a putt. Distance: " + distance);
								distance /= 2;
							}
							
							
						}
						
						// If the club is a pitching wedge, assume the shot goes to the edge of the green for now
						// Replace with a method for later
						else if (club.equals("Pitching Wedge"))
						{
							//System.out.println("Pitching Wedge from " + distance + " yards away" );
							// Once the ball hits the edge of the green, have it "roll" a random distance
							roll = 3 + r.nextInt(11);
							
							// Set the distance to 15 + the roll
							shotStatement.setString(10, Integer.toString(distance - 15 + roll));
							distance = 15 - roll;
							// Assume it was taken on the fairway
							shotStatement.setString(11, "Fairway");
						}
						// If the club is anything greater than a 9 iron
						else
						{
							// TO DO: Change logic around clubs are chosen
							// Driver should only be the first shot
							
							// Assume the shot was hit at the maximum range[
							shotDistance = clubMap.get(club);
							// Decrement the shot distance by a random amount from 0,30 to create variability
							shotDistance -= r.nextInt(30);
							shotStatement.setString(10, Integer.toString(shotDistance));
							distance -= shotDistance;
							shotStatement.setString(11, "Fairway");

						}
						// Insert Shot Data into the list for the round, update EndDistance (9), update the Holes and Par strings for Round
						shotStatement.setString(9, Integer.toString(distance));
						
						
						shotStatements.add(shotStatement);
						
					}					
					score += shots;
					totalPar += par;
					// Create the comma seperated list of holes, score and par data.  
					holesRoundValue += (j+1);
					scoresRoundValue += shots;
					parRoundValue += par;

					// Add a comma if it's not the last hole
					if (j < (numHoles - 1))
					{
						holesRoundValue += ",";
						scoresRoundValue += ",";
						parRoundValue += ",";
					}
				}
				// Create RoundID from userID + timestamp
				roundID = userID + "+" +  new java.text.SimpleDateFormat("MM/dd/yyyy h:mm:ss a").format(new java.util.Date());
				roundStatement=con.prepareStatement("INSERT INTO Round(ID,UserID,CourseName,Par,Score,TotalPar,TotalScore,startHole, TotalHoles,Date) VALUES (?,?,?,?,?,?,?,?,?,?);");
				roundStatement.setString(1, roundID);
				roundStatement.setInt(2, userID);
				roundStatement.setString(3, courseName);
				roundStatement.setString(4,  parRoundValue);
				roundStatement.setString(5,  scoresRoundValue);
				roundStatement.setInt(6, totalPar);
				roundStatement.setString(7, Integer.toString(score));
				roundStatement.setInt(8, 1);
				roundStatement.setInt(9, 18);
				roundStatement.setDate(10, date);
			
				//Debug statement for Round
				System.out.println("INSERT INTO Round(ID,UserID,CourseName,Holes,Par,Score,TotalScore,Date) VALUES (?,?,?,?,?,?,?,?)");
				System.out.println("Round Statement: " + roundStatement);
				
				roundStatement.executeUpdate();
				
				System.out.println("Round Statement Executed");
				
				// Execute all the shot statements for the current round after the Round entry has been created
				// Add RoundID in at the end
				for (int s = 0; s < shotStatements.size(); s ++)
				{
					shotStatements.get(s).setString(1, roundID);
					shotStatements.get(s).executeUpdate();
					//System.out.println(shotStatements.get(s));
				}
				
				//rs = st.executeQuery("SELECT Hole, Stroke, Club, Yardage FROM Shot WHERE RoundID like \"" + roundID + "\"");
				
				while(rs.next())
				{
					int zzz = 0;
					System.out.println("Round: "+ roundID + " Hole: " + rs.getString(1) + " Stroke: " + rs.getString(2) + " Club: " + rs.getString(3) + " Distance " + rs.getString(4));
				}
				System.out.println();


				
			}
			
		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(CreateTestRoundData.class.getName());
		    lgr.log(Level.SEVERE, ex.getMessage(), ex);
	    	System.out.println("Error connecting to SQL or executing query");
	
	    } finally {
	        try { 
	            if (st != null) {
	                st.close();
	            }
	            if (con != null) {
	                con.close();
	            }
	
	        } catch (SQLException ex) {
	        	System.out.println("Error connecting to SQL or executing query");
	        	}
	       }
	}
	
	// Create the mapping of club data and distances that will be used to simulate the round of golf
	public static void addClubData(Map<String,Integer> clubMap, Map<Integer,Double> putterMap)
	{
		// Add clubs and distances
		clubMap.put("Driver", 230);
		clubMap.put("3 Wood", 215);
		clubMap.put("5 Wood", 195);
		clubMap.put("2 Iron", 195);
		clubMap.put("3 Iron", 180);
		clubMap.put("4 Iron", 170);
		clubMap.put("5 Iron", 160);
		clubMap.put("6 Iron", 150);
		clubMap.put("7 Iron", 140);
		clubMap.put("8 Iron", 130);
		clubMap.put("9 Iron", 115);
		clubMap.put("Pitching Wedge", 105);
		
		// Add putting probablities and distance
		putterMap.put(15,.2);
		putterMap.put(14, .3);
		putterMap.put(13, .4);
		putterMap.put(12, .5);
		putterMap.put(11, .6);
		putterMap.put(10, .7);
		putterMap.put(9, .8);
		putterMap.put(6, .9);
		putterMap.put(3, .95);
		// In the leather
		putterMap.put(2, 1.0);
	}
	
	
	// Choose a random date and return it in MYSQL format
	public static Date randomDate(int baseYear, int yearRange)
	{
		Random r = new Random();
		// Generate a year between baseYear and baseYear + yearRange;
		int randomYear = r.nextInt(yearRange) + (baseYear - yearRange + 1);
		// Generate a number between 1 and 365
		int dayOfYear = r.nextInt(365) + 1; 
		
		// Get a calendar and create a date object based on the random values above
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, randomYear);
		calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
		java.util.Date randomDoB = calendar.getTime();
		
		// Haha returnDate
		// Add the components of the date 
		Date returnDate = new java.sql.Date(randomDoB.getTime());
		System.out.println("Return Date: " + returnDate);
		return returnDate;
		
	}
	
	
	// Based on a given distance, choose the club to use based on lookup table
	// Design
	// 1st shot - Use Driver unless distance is too far (Accuracy - % based on rough (.8 multiplier) vs fairway (1.0 multiplier) vs tee (1.1 multiplier)
	// 2nd-n shot, use the highest iron available
	// Within range of pitching wedge - use that
	// On the green, use a putter
	
	public static String selectClub(int distance, Map<String, Integer> clubMap)
	{
		// If that ball is "on the green" use a putter
		if (distance <= 15)
			return "Putter";
		String club = "";
		
		// Create a set of the clubs is descending order of range
		Set<String> clubs = clubMap.keySet();
		Iterator iter = clubs.iterator();
		
		// Go through all clubs to find the one with the highest range that does not exceed the distance to the whole 
		while (iter.hasNext())
		{
			club = (String) iter.next();

			if (clubMap.get(club) < distance)
				return club;
		}
		
		// If the range for all clubs exceed distance, then use the pitching wedge 
		return "Pitching Wedge";
	}
	
	
}


