import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ViewTestData {
	
	public static void main(String[] args) throws Exception 
	{
        createRoundData(1);	
	}
	
	public static void createRoundData (int entries)
	{
		
		// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		// SQL Objects
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement roundStatement = null, shotStatement = null;

		// The List of all available UserIDs in the database
		List<String> userIDs = new ArrayList<String>();
		// The list of all available CourseIDs in the database
		List<String> courseIDs = new ArrayList<String>();
		
		// Create an instance of the test round class to use the getID met
		CreateTestRoundData ctrd = new CreateTestRoundData();
		
		// Queries to Execute
		String averageScoreQuery = 
			  "SELECT U.FirstName, U.LastName, R.avg_score "
			 + "FROM (SELECT UserID, AVG(Score) as avg_score FROM Round GROUP BY UserID ORDER BY avg_score ASC) as R "
			 + "Left JOIN UserProfile U ON R.UserID = U.UserID";
		
		String averageUserScorebyCourseQuery = 
				"SELECT U.FirstName, U.LastName, Z.CourseName, Z.avg_score, Z.num_rounds "
				+ "FROM (SELECT R2.UserID, C.CourseName, R2.avg_score, R2.num_rounds "
				+ "FROM (SELECT UserID, CourseID, AVG(Score) as avg_score, Count(CourseID) as num_rounds FROM Round GROUP BY UserID, CourseID) as R2 "
				+ "LEFT JOIN Course C ON C.ID = R2.CourseID) as Z "
				+ "LEFT JOIN UserProfile U ON Z.UserID = U.UserID";
		
		String selectStarFromRoundQuery = 
			  " SELECT UserID, Score, count(*) FROM Round GROUP BY UserID";
		
		
		
		try{
			// Create the SQL Connection
	        con = DriverManager.getConnection(url + dbName, userName, password);
	       
	        // Call methods to return the queries defined in the top of the class
			
			st = con.createStatement();
		    rs = st.executeQuery(averageScoreQuery);
		    
		    while (rs.next())
		    {
		    	System.out.println("User: " + rs.getString(1) +  " " + rs.getString(2) + " Score: " + rs.getString(3));
		    }
		    
		    System.out.println();
		    st = con.createStatement();
		    rs = st.executeQuery(averageUserScorebyCourseQuery);
		    System.out.println("Average Score for all users, by course");
		    while (rs.next())
		    {
		    	System.out.println("" + rs.getString(1) + " " + rs.getString(2) + " || " + rs.getString(3) + " || Average Score: " + rs.getString(4) + " || Rounds: " + rs.getString(5) );
		    }
		    System.out.println();
		    st = con.createStatement();
		    rs = st.executeQuery(selectStarFromRoundQuery);
		    
		    while (rs.next())
		    {
		    	System.out.println("UserID: " + rs.getString(1) + " Score: " + rs.getString(2) + " Count: " + rs.getString(3));
		    }
		    System.out.println();
		    /*
		    st = con.createStatement();
		    rs = st.executeQuery(joinQuery);
		    
		    while (rs.next())
		    {
		    	System.out.println("User: " + rs.getString(1) + " " + rs.getString(2) + " Rounds: " + rs.getString(3));
		    }
		    System.out.println();
		   */
		    
			


		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(CreateTestRoundData.class.getName());
		    lgr.log(Level.SEVERE, ex.getMessage(), ex);
	    	System.out.println("Error connecting to SQL or executing query");
	
	    } finally {
	        try { 
	            if (st != null) {
	                st.close();
	            }
	            if (con != null) {
	                con.close();
	            }
	
	        } catch (SQLException ex) {
	        	System.out.println("Error connecting to SQL or executing query");
	        	}
	       }
	}

}
